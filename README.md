# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repo will contain all code related to create a HTML page with CSS 
* Version 1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* we will start by creating branches for the codes we need to creat 
* 1.I have created Main Repository called " noc_repo "
* 2.every member will need to create a branch from this repo
* 3.every member will commit the changes on that branch
* 4.every member after committing changes will need to create a pull request 
* 5.lead will merge these commits 
* 6.one member will deploy the new changes through Jenkins on EC2 instance 
* check the output on the server 

* Deploymnet steps:
* Using EC2 on the AWS account, we will merge the code and push it with a Jenkins job, each member has to create a jenkins job and write it to take all
* the codes from this main repo and push it to the server 

##### Branches that needs to be created ####

* •	Branch to setup a background image to the page --> Dania 
* •	Branch to ADD website Title in the page --> Dania 
* •	Branch to ADD video player in the page from YouTube --> Nizar
	
* Branch to ADD Counter to the page --> Nizar
* •	Branch to ADD google play store and apple store pointed to some example app --> Sami
* •	Branch to ADD button redirecting this page to another website ---> Sami
* •	Branch to ADD search bar in HTML ---> Ahmad
* •	Branch to ADD Images with border in HTML ---> Ahmad 


### Contribution guidelines ###

* your code will be tested and checked as from the output on the server

### Who do I talk to? ###

* Repo owner or admin, you can refer back to Nisreen or osama 
